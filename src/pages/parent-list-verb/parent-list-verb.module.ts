import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentListVerbPage } from './parent-list-verb';

@NgModule({
  declarations: [
    ParentListVerbPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentListVerbPage),
  ],
})
export class ParentListVerbPageModule {}
