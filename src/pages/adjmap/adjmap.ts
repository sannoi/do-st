import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { RestProvider } from '../../providers/rest/rest';

declare var google: any;
/**
 * Generated class for the AdjmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adjmap',
  templateUrl: 'adjmap.html',
})
export class AdjmapPage {

  @ViewChild("map") mapElement;
  map: any;
  stdid: any;
  userid: any;
  student_id: any;

  latitude: any;
  longtitude: any;

  tmp: any;


  marker_latitude: any;
  marker_longtitude: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public geolocation: Geolocation,
    public restProvider: RestProvider,
    public alertCtrl: AlertController) {

    this.userid = navParams.get('userid');

  }

  ionViewDidLoad() {

    this.restProvider.getInfo(this.userid).subscribe(result => {
      this.student_id = result.student_id;
      this.latitude = result.latitude;
      this.longtitude = result.longtitude;
      this.initMap();
    });

  }

  ionViewDidEnter() {

  }

  initMap() {

    console.log(this.latitude);
    if (this.latitude) {

      console.log("Retrieve Map");
      this.marker_latitude = this.latitude
      this.marker_longtitude = this.longtitude

      let coords = new google.maps.LatLng(this.latitude, this.longtitude);
      let mapOptions: google.maps.MapOptions = {
        center: coords,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions)

      let marker: google.maps.Marker = new google.maps.Marker({
        map: this.map,
        position: coords
      })
    }
    else {
      console.log("No Retrieve Map");
      this.geolocation.getCurrentPosition().then((position) => {

        let coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        let mapOptions: google.maps.MapOptions = {
          center: coords,
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions)

        // let marker: google.maps.Marker = new google.maps.Marker({
        //   map: this.map,
        //   position: coords
        // })


      }, (err) => {
        console.log(err);
      });

    }



  }

  addMarker() {

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter(),
      draggable: true
    });

    let content = "<h4>" + this.student_id + "</h4>";

    this.addInfoWindow(marker, content);

    this.marker_latitude = marker.getPosition().lat();
    this.marker_longtitude = marker.getPosition().lng();
  }

  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });



    google.maps.event.addListener(marker, 'click', () => {

      infoWindow.open(this.map, marker);

      this.marker_latitude = marker.getPosition().lat();
      this.marker_longtitude = marker.getPosition().lng();

    });

    google.maps.event.addListener(marker, 'dragend', () => {

      infoWindow.open(this.map, marker);

      // this.markerlatlong = marker.getPosition();
      this.marker_latitude = marker.getPosition().lat();
      this.marker_longtitude = marker.getPosition().lng();

      // console.log("latlong   " + this.markerlatlong);
      console.log("lat    " + this.marker_latitude);
      console.log("long   " + this.marker_longtitude);
    });

  }

  setmap() {

    // this.presentAlert(this.marker_latitude);

    this.restProvider.uploadLocation(this.userid, this.marker_latitude, this.marker_longtitude).subscribe(result => {

      console.log(result.desc);
      if (result.desc == "success") {
        this.presentAlert("บันทึกสถานที่ตั้งเสร็จสิ้น");
        this.navCtrl.setRoot("HomePage");
      }
    });

  }


  async presentAlert(message) {
    const alert = await this.alertCtrl.create({
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }



}
