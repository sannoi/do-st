import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SdqPage } from './sdq';

@NgModule({
  declarations: [
    SdqPage,
  ],
  imports: [
    IonicPageModule.forChild(SdqPage),
  ],
})
export class SdqPageModule {}
