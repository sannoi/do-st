import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailHomeworkPage } from './detail-homework';

@NgModule({
  declarations: [
    DetailHomeworkPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailHomeworkPage),
  ],
})
export class DetailHomeworkPageModule {}
