import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the EnglishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-english',
  templateUrl: 'english.html',
})
export class EnglishPage {

  courseid: any;
  course_name: any;

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  IsEng: any;
  teacherid: any;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage) {

    this.courseid = navParams.get('courseid');
    this.course_name = navParams.get('course_name');
    this.IsEng = navParams.get('IsEng');
    this.teacherid = navParams.get('teacherid');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnglishPage');
  }

  ionViewDidEnter() {
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
          })
        })
      })
    })
  }

  verb() {
    this.navCtrl.push("VerbPage");
  }

  ListVerbPage() {
    this.navCtrl.push("ListVerbPage", { 'courseid': this.courseid });
  }


  detail_eng() {
    this.navCtrl.push("DetailEnglishPage", { 'courseid': this.courseid, 'course_name': this.course_name });
  }

  back() {
    this.navCtrl.setRoot("HomeworkPage");
  }

  IdTeacherPage() {
    console.log(this.courseid);
    this.navCtrl.push("IdTeacherPage", { 'courseid': this.courseid });
  }

  totolscore() {
    this.navCtrl.push("TotalScorePage", { 'courseid': this.courseid, 'course_name': this.course_name, 'teacherid': this.teacherid });
  }

  VocabularyPage() {
    this.navCtrl.push("ListVocabPage", { 'courseid': this.courseid });
  }

  new_HomeworkPage() {
    // console.log(this.courseid);
    this.navCtrl.push("NewHomeworkPage", { 'courseid': this.courseid, 'schoolid': this.schoolid, 'userid': this.userid });

  }



}
