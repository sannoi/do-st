import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentNewsPage } from './parent-news';

@NgModule({
  declarations: [
    ParentNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentNewsPage),
  ],
})
export class ParentNewsPageModule {}
