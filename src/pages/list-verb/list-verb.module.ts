import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListVerbPage } from './list-verb';

@NgModule({
  declarations: [
    ListVerbPage,
  ],
  imports: [
    IonicPageModule.forChild(ListVerbPage),
  ],
})
export class ListVerbPageModule {}
