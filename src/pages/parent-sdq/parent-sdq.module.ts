import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentSdqPage } from './parent-sdq';

@NgModule({
  declarations: [
    ParentSdqPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentSdqPage),
  ],
})
export class ParentSdqPageModule {}
