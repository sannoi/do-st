import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CheckNameProvider } from '../../providers/check-name/check-name';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ChecknamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkname',
  templateUrl: 'checkname.html',
})
export class ChecknamePage {

  courseid: any;
  teacherid: any;
  roomid: any;
  userid: any;
  student: any;
  // studentList: any;
  student_img: any;
  studentList: Array<any> = []
  studentList_nor: Array<any> = []

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public _serv: CheckNameProvider,
    private storage: Storage,
    public loadingController: LoadingController) {

    this.courseid = this.navParams.get('courseid');
    this.teacherid = this.navParams.get('teacherid');
  }


  async presentLoadingWithOptions_fail() {
    const loading = await this.loadingController.create({
      spinner: null,
      content: "ไม่สามารถบันทึกได้",
      duration: 2500,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async presentLoadingWithOptions_done() {
    const loading = await this.loadingController.create({
      spinner: null,
      content: "บันทึกเรียบร้อย",
      duration: 2500,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  back() {
    this.navCtrl.setRoot("HomeworkPage");
  }

  saveCheck() {
    const loading = this.loadingController.create({
      spinner: null,
      content: "รอสักครู่",
      cssClass: 'custom-class custom-loading'
    });

    if (this.studentList.length == 0 && this.studentList_nor.length == 0) {
      this.navCtrl.setRoot("HomeworkPage");
    }
    else {

      loading.present();
      var num: number = 5;
      var i: number;

      if (this.studentList_nor.length != 0) {
        for (i = 0; i < this.studentList.length; i++) {
          this._serv.UpdateCheckName_class(this.studentList[i], "y", this.courseid).subscribe(result => { })
        }
        this.studentList = [];
      }

      if (this.studentList_nor.length != 0) {
        for (i = 0; i < this.studentList_nor.length; i++) {
          this._serv.UpdateCheckName_class(this.studentList_nor[i], "n", this.courseid).subscribe(result => { })
        }
        this.studentList_nor = [];
      }

      loading.dismiss();
      this.presentLoadingWithOptions_done();
      this.ionViewDidLoad();
      this.navCtrl.setRoot("HomeworkPage");

    }
  }

  ionViewDidLoad() {

    this.storage.get('student_img').then((val) => { this.student_img = val })
    this.storage.get('userid').then((val) => { this.userid = val; })

    this.storage.get('roomid').then((val) => {
      this.roomid = val

      this._serv.getStudentList_chk(this.roomid, this.teacherid, this.courseid).subscribe(result => {
        console.log(result);
        this.student = result

        this.student.forEach(element => {
          if (element.IsComing == "y") {
            this.studentList.push(element.id);
          }
          else {
            this.studentList_nor.push(element.id);
          }

        });
        console.log("com" + this.studentList);
        console.log("no" + this.studentList_nor);

      })
    })


  }

  onSaveComing(value: boolean, x) {

    if (value == true) {
      const index: number = this.studentList_nor.indexOf(x);
      if (index !== -1) {
        this.studentList_nor.splice(index, 1);
      }
      this.studentList.push(x);
    }
    else {
      const index: number = this.studentList.indexOf(x);
      if (index !== -1) {
        this.studentList.splice(index, 1);
      }
      this.studentList_nor.push(x);

    }


    console.log(this.studentList)

  }





}
