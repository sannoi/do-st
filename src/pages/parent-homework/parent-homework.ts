import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { course_list } from '../../models/course_list';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ParentHomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-homework',
  templateUrl: 'parent-homework.html',
})
export class ParentHomeworkPage {

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  courseList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider,
    public http: HTTP, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentHomeworkPage');
  }

  cList: course_list;
  ionViewDidEnter() {
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.restProvider.getCourseList(this.userid, this.schoolid).subscribe(result => {
              this.courseList = result;
              console.log('course list', result)
            });
          })
        })
      })
    })
  }

  detail_Click(courseid, status_eng, course_name, teacherid) {
    this.navCtrl.push("ParentEnglishPage", { 'courseid': courseid, 'course_name': course_name, 'IsEng': status_eng, "teacherid": teacherid });
  }


}
