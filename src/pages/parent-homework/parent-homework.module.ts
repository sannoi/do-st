import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentHomeworkPage } from './parent-homework';

@NgModule({
  declarations: [
    ParentHomeworkPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentHomeworkPage),
  ],
})
export class ParentHomeworkPageModule {}
