import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentDetailHomeworkPage } from './parent-detail-homework';

@NgModule({
  declarations: [
    ParentDetailHomeworkPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentDetailHomeworkPage),
  ],
})
export class ParentDetailHomeworkPageModule {}
