import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the ChangepassMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepass-menu',
  templateUrl: 'changepass-menu.html',
})
export class ChangepassMenuPage {

  [x: string]: any;

  username: string;
  tel: string;
  newpassword: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public AlertController: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepassMenuPage');
  }

  change_password() {

    this.restProvider.change_password(this.username, this.tel, this.newpassword).subscribe(async (result) => {
      console.log(result);
      console.log(this.username);
      console.log(this.tel);
      console.log(this.newpassword);

      if (result.returncode == "3810") {
        let alert = this.AlertController.create({
          message: result.desc,
          buttons: ['รับทราบ']
        })
        alert.present();
      } else if (result.returncode == "3800") {
        let alert = this.AlertController.create({
          message: result.desc,
          buttons: ['รับทราบ']
        })
        alert.present();
      } else if (result.returncode == "1000") {
        let alert = this.AlertController.create({
          message: "เปลี่ยนรหัสผ่านใหม่สำเร็จ",
          buttons: ['รับทราบ']
        })
        alert.present();
        this.navCtrl.setRoot("HomePage");
      }

    });
  }


}
