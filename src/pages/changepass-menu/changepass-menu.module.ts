import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangepassMenuPage } from './changepass-menu';

@NgModule({
  declarations: [
    ChangepassMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangepassMenuPage),
  ],
})
export class ChangepassMenuPageModule {}
