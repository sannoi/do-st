import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentVerbPage } from './parent-verb';

@NgModule({
  declarations: [
    ParentVerbPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentVerbPage),
  ],
})
export class ParentVerbPageModule {}
