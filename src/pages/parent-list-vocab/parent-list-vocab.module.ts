import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentListVocabPage } from './parent-list-vocab';

@NgModule({
  declarations: [
    ParentListVocabPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentListVocabPage),
  ],
})
export class ParentListVocabPageModule {}
