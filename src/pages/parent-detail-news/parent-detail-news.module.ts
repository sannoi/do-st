import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentDetailNewsPage } from './parent-detail-news';

@NgModule({
  declarations: [
    ParentDetailNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentDetailNewsPage),
  ],
})
export class ParentDetailNewsPageModule {}
