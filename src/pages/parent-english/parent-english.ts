import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ParentEnglishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parent-english',
  templateUrl: 'parent-english.html',
})
export class ParentEnglishPage {

  courseid: any;
  course_name: any;

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  IsEng: any;
  teacherid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage) {

    this.courseid = navParams.get('courseid');
    this.course_name = navParams.get('course_name');
    this.IsEng = navParams.get('IsEng');
    this.teacherid = navParams.get('teacherid');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParentEnglishPage');
  }


  ionViewDidEnter() {
    this.storage.get('student_img').then((val) => {
      this.student_img = val;
      this.storage.get('student_level').then((val) => {
        this.student_level = val;
        this.storage.get('userid').then((val) => {
          this.userid = val;
          this.storage.get('schoolid').then((val) => {
            this.schoolid = val;
          })
        })
      })
    })
  }

  verb() {
    this.navCtrl.push("VerbPage");
  }

  ListVerbPage() {
    this.navCtrl.push("ParentListVerbPage", { 'courseid': this.courseid });
  }


  detail_eng() {
    this.navCtrl.push("ParentDetailEnglishPage", { 'courseid': this.courseid, 'course_name': this.course_name });
  }

  back() {
    this.navCtrl.setRoot("ParentHomeworkPage");
  }

  totolscore() {
    this.navCtrl.push("ParentTotalScorePage", { 'courseid': this.courseid, 'course_name': this.course_name, 'teacherid': this.teacherid });
  }

  VocabularyPage() {
    this.navCtrl.push("ParentListVocabPage", { 'courseid': this.courseid });
  }

}
