import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllHomeworkPage } from './all-homework';

@NgModule({
  declarations: [
    AllHomeworkPage,
  ],
  imports: [
    IonicPageModule.forChild(AllHomeworkPage),
  ],
})
export class AllHomeworkPageModule {}
