import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { HTTP } from '../../../node_modules/@ionic-native/http';
import { Storage } from '@ionic/storage';
import { course_list } from '../../models/course_list';

/**
 * Generated class for the HomeworkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homework',
  templateUrl: 'homework.html',
})
export class HomeworkPage {

  userid: any;
  schoolid: any;
  student_level: string;
  student_img: string;
  courseList: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public restProvider: RestProvider,
    public http: HTTP, private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeworkPage');
  }

  cList: course_list;
  ionViewDidEnter() {
    this.storage.get('userid').then((val) => {
      this.userid = val;
      this.storage.get('schoolid').then((val) => {
        this.schoolid = val;
        this.storage.get('student_img').then((val) => {
          this.student_img = val;
          this.storage.get('student_level').then((val) => {
            this.student_level = val;
            this.restProvider.getCourseList(this.userid, this.schoolid).subscribe(result => {
              this.courseList = result;
              console.log('course list', result)
            });
          })
        })
      })
    })
  }

  detail_Click(courseid, status_eng, course_name, teacherid) {
    this.navCtrl.push("EnglishPage", { 'courseid': courseid, 'course_name': course_name, 'IsEng': status_eng, "teacherid": teacherid });
  }



}
