import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParentAllHomeworkPage } from './parent-all-homework';

@NgModule({
  declarations: [
    ParentAllHomeworkPage,
  ],
  imports: [
    IonicPageModule.forChild(ParentAllHomeworkPage),
  ],
})
export class ParentAllHomeworkPageModule {}
